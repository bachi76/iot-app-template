module.exports = {
  "prompts": {
    "name": {
      "type": "string",
      "required": true,
      "message": "Project name",
      "store": true
    },
    "description": {
      "type": "string",
      "required": true,
      "message": "Project description",
      "default": "A vuetify-based IoT App",
      "store": true
    },
    "author": {
      "type": "string",
      "message": "Author",
      "store": true
    },
    "webappPath": {
      "type": "string",
      "required": true,
      "message": "Webapp url path (used as deployment path, e.g. '/apps/webapp-name'. Must start with /"      
    },   
      "deviceHost": {
      "type": "string",
      "message": "Default IoT device ip or host name and websocket port",
      "default": "192.168.4.1:81",
      "store": true
    }
  },
  "showTip": true,
  "gitInit": true,
  "installDependencies": true
}
