# <%= name %>

> <%= description %>

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
## Deployment
Uses IntelliJ IDEA. Preconfigured to use my webserver, adjust deployment setting for your own. Deploys `index.html` and the `dist` folder to:

[brain.vollmilch.ch:4567<%= webappPath %>/](brain.vollmilch.ch:4567<%= webappPath %>/)

## Development
- Access server vars from pages: `this.$parent.server`

## Related links
* [iot-webapp-template](https://bitbucket.org/bachi76/iot-app-template)
* [iot-server](https://bitbucket.org/bachi76/iot-server)
* [esp8266-framework](https://bitbucket.org/bachi76/esp8266-framework)
* [Vue.js 2 Guide](https://vuejs.org/v2/guide/)
* [Vue Lifecycle hooks](https://alligator.io/vuejs/component-lifecycle/)
* [Vuetify Guide](https://vuetifyjs.com/vuetify/quick-start)
* [Hermit](https://play.google.com/store/apps/details?id=com.chimbori.hermitcrab) (create mobile lite apps from webpages) 
 