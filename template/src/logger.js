/**
 * Logger interface. Logs to console by default.
 * Set an onEntry(entry) event handler to further process log entries.
 */
import * as moment from 'moment';
export default function Logger(eventHandler) {
    this.onEntry = eventHandler;

    this.Entry = function (origin, severity, msg) {
        this.timestamp = moment().format('DD.MM.YY HH:mm:ss');
        this.origin = origin || 'Client';
        this.severity = severity;
        this.msg = msg;
    };
    this.i = function(msg, origin) {
        if (!origin) origin = "";
        if(this.onEntry) this.onEntry(new this.Entry(origin, "Info", msg));
        console.log(origin + ' ' + msg);
    };
    this.w = function(msg, origin) {
        if (!origin) origin = "";
        if(this.onEntry) this.onEntry(new this.Entry(origin, "Warn", msg));
        console.warn(origin + ' ' + msg);
    };
    this.e = function(msg, origin) {
        if (!origin) origin = "";
        if(this.onEntry) this.onEntry(new this.Entry(origin, "Error", msg));
        console.error(origin + ' ' + msg);
    };
    this.a = function(msg, origin) {
        if (!origin) origin = "";
        if(this.onEntry) this.onEntry(new this.Entry(origin, "ALERT", msg));
        console.error(origin + ' ' + msg);
    };
}