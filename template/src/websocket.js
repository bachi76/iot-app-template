/**
 * WebSockets - returns the connection object
 */
import Vue from 'vue';

export default function initWS(host) {

    log.i("Connecting to " + host + "...");
    var connection = new WebSocket(host);
    connection.lastPingReceived = 0;

    // When the connection is open, send some data to the server
    connection.onopen = function () {
        console.log("Connected!");
        connection.lastPingReceived = Date.now();
        app.client.connected = true;
    };

    connection.onclose = function (event) {
        log.e('Connection closed (reason: ' + event.code + ' ' + event.reason + ')');
        app.client.connected = false;
    };

    // Log errors
    connection.onerror = function (error) {
        log.e('WebSocket error: ' + error.message);
        connection.close();
    };

    // Log messages from the server
    connection.onmessage = function (e) {

        // RSSI messages act as heartbeat signal
        if (e.data.indexOf('rssi') !== -1) {
            connection.lastPingReceived = Date.now();
        }

        console.log("Server sent: " + e.data);
        //log.i("Server sent: " + e.data);

        var payload = JSON.parse(e.data);

        for(var key in payload){
            if(payload.hasOwnProperty(key)){

                // We need to convert the server data to proper Vue.set() calls in order
                // to keep the vue property observer mechanism alive.
                //
                // This: {"plants[1].light":"1051"}
                // becomes this: Vue.set(vm.server.plants[1], 'light', payload['plants[1].light'])
                // and
                // This: {"channels":"2"}
                // becomes this: Vue.set(vm.server, 'channels', payload['channels'])

                var obj = 'vm.server';
                var key2 = key;
                var pos = key.lastIndexOf(".");
                if (pos > -1) {
                    obj = obj + "." + key.substring(0, pos);
                    key2 = key.substring(pos + 1);
                }

                Vue.set(app.server, key2, payload[key]);
            }
        }

        //vm.$forceUpdate();
    };

    window.addEventListener("beforeunload", function(e){
        connection.close();
    }, false);

    return connection;
}