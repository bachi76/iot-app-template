import Vue from 'vue';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import App from './App.vue';

import HomePage from './pages/HomePage.vue';
import ConfigPage from './pages/ConfigPage.vue'
import AboutPage from './pages/AboutPage.vue';
import LogPage from './pages/LogPage.vue';
import LoginDialog from './components/LoginDialog.vue'

import Logger from './logger.js';

const routes = [
    {
        path: '/home',
        name: 'home',
        component: HomePage,
    },
    {
        path: '/config',
        name: 'config',
        component: ConfigPage,
    },
    {
        path: '/log',
        name: 'log',
        component: LogPage,
    },
    {
        path: '/about',
        name: 'about',
        component: AboutPage,
    },
    {
        path: '*',
        redirect: {
            name: 'home',
        },
    },
];
const router = new VueRouter({
    routes,
    root: '/home',
});

// Enable app logging (gloabl log is defined in index.html)
log = new Logger();

Vue.use(Vuetify);
Vue.use(VueRouter);

// Globally available component
Vue.component('login-dialog', LoginDialog);

new Vue({ // eslint-disable-line no-new
    el: '#app',
    router,
    components: {
        LoginDialog: 'login-dialog'
    },
    render: h => h(App)
});

log.i("Initialized! ", "client");


