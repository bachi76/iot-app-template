# iot-app-template

> A simple Vue & vuetify & webpack app template using Bachi's iot platform

This is the web app part. You'll also want the [ESP8266 counterpart](https://bitbucket.org/bachi76/esp_app_template)


### Installation

This is a project template to be used with [Sao](https://sao.js.org).

``` bash
$ npm install -g sao
$ sao --install=true bitbucket:bachi76/iot-app-template my-project
$ cd my-project
$ npm run dev
```

### Usage

- `npm run dev`: Webpack + `vue-loader` with proper config for source maps & hot-reload.

- `npm run build`: build with HTML/CSS/JS minification.

- Deployment using IntelliJ IDEA: Preconfigured to use my webserver, adjust deployment setting for your own. Deploys `index.html` and the `dist` folder.

### Adjusting the template

- Edit `sao.js`, these variables are parsed into all files in the `template` folder (using EJS syntax, <%= varname %>).

- IDE: Open the template folder in IDEA



### Q & A

Q: Why are you using sao and not vue-cli?
A: Because the `{{}}` template markers of the scaffolding and vue itself [collide](https://github.com/vuejs/vue-cli/issues/284#issuecomment-319675420)

